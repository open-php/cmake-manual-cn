# cmake-env-variables(7)
## Contents

cmake-env-variables(7)

Environment Variables that Change Behavior

Environment Variables that Control the Build

Environment Variables for Languages

Environment Variables for CTest

Environment Variables for the CMake curses interface

This page lists environment variables that have special meaning to CMake.

For general information on environment variables, see the Environment Variables section in the cmake-language manual.

Environment Variables that Change Behavior
CMAKE_PREFIX_PATH
SSL_CERT_DIR
SSL_CERT_FILE
Environment Variables that Control the Build
ADSP_ROOT
CMAKE_APPLE_SILICON_PROCESSOR
CMAKE_BUILD_PARALLEL_LEVEL
CMAKE_BUILD_TYPE
CMAKE_COLOR_DIAGNOSTICS
CMAKE_CONFIGURATION_TYPES
CMAKE_CONFIG_TYPE
CMAKE_EXPORT_COMPILE_COMMANDS
CMAKE_GENERATOR
CMAKE_GENERATOR_INSTANCE
CMAKE_GENERATOR_PLATFORM
CMAKE_GENERATOR_TOOLSET
CMAKE_INSTALL_MODE
CMAKE_<LANG>_COMPILER_LAUNCHER
CMAKE_<LANG>_LINKER_LAUNCHER
CMAKE_MSVCIDE_RUN_PATH
CMAKE_NO_VERBOSE
CMAKE_OSX_ARCHITECTURES
CMAKE_TOOLCHAIN_FILE
DESTDIR
LDFLAGS
MACOSX_DEPLOYMENT_TARGET
<PackageName>_ROOT
VERBOSE
Environment Variables for Languages
ASM<DIALECT>
ASM<DIALECT>FLAGS
CC
CFLAGS
CSFLAGS
CUDAARCHS
CUDACXX
CUDAFLAGS
CUDAHOSTCXX
CXX
CXXFLAGS
FC
FFLAGS
HIPCXX
HIPFLAGS
ISPC
ISPCFLAGS
OBJC
OBJCXX
RC
RCFLAGS
SWIFTC
Environment Variables for CTest
CMAKE_CONFIG_TYPE
CTEST_INTERACTIVE_DEBUG_MODE
CTEST_NO_TESTS_ACTION
CTEST_OUTPUT_ON_FAILURE
CTEST_PARALLEL_LEVEL
CTEST_PROGRESS_OUTPUT
CTEST_USE_LAUNCHERS_DEFAULT
DASHBOARD_TEST_FROM_CTEST
Environment Variables for the CMake curses interface
CCMAKE_COLORS
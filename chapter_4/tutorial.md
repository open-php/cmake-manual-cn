# CMake Tutorial
## Introduction
The CMake tutorial provides a step-by-step guide that covers common build system issues that CMake helps address. Seeing how various topics all work together in an example project can be very helpful.

Steps
The tutorial source code examples are available in this archive. Each step has its own subdirectory containing code that may be used as a starting point. The tutorial examples are progressive so that each step provides the complete solution for the previous step.

Step 1: A Basic Starting Point
Exercise 1 - Building a Basic Project
Exercise 2 - Specifying the C++ Standard
Exercise 3 - Adding a Version Number and Configured Header File
Step 2: Adding a Library
Exercise 1 - Creating a Library
Exercise 2 - Making Our Library Optional
Step 3: Adding Usage Requirements for a Library
Exercise 1 - Adding Usage Requirements for a Library
Step 4: Adding Generator Expressions
Exercise 1 - Setting the C++ Standard with Interface Libraries
Exercise 2 - Adding Compiler Warning Flags with Generator Expressions
Step 5: Installing and Testing
Exercise 1 - Install Rules
Exercise 2 - Testing Support
Step 6: Adding Support for a Testing Dashboard
Exercise 1 - Send Results to a Testing Dashboard
Step 7: Adding System Introspection
Exercise 1 - Assessing Dependency Availability
Step 8: Adding a Custom Command and Generated File
Step 9: Packaging an Installer
Step 10: Selecting Static or Shared Libraries
Step 11: Adding Export Configuration
Step 12: Packaging Debug and Release